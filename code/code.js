/* Проверка данных. Используя JS cоздайте 5 полей для ввода данных.
Добавьте стили на ошибку и стиль на верный ввод. Поля: Имя (Укр. Буквы), Номер телефона в формате +38ХХХ-ХХХ-ХХ-ХХ, 
електронная почта, пароль и подтверждение пароля (пароли должны совпадать). Реализуй проверку данных. 
При вводе данных сразу проверять на правильность и выводить ошибку если такая необходима. Добавь кнопку регистрация, 
при нажатии кнопки проверить все поля и вывести ошибку если такая будет. Сохраниение. Если пользователь все записал 
верно, то сохраните данные на клиенте с указанием даты и времени сохранения. Для стилизации используй CSS классы.
Для создания элементов используй JS. */



window.addEventListener("DOMContentLoaded", () => {
    const root = document.getElementById("root");
    const output = document.getElementById("output");
    let validPass = false,
        validInput = false;

    // создали переменные для элементов
    const elements = function (type, placeholder = "", className = "", pattern) {
        const input = document.createElement("input");
        const label = document.createElement("label");
        const div = document.createElement("div");
        const div2 = document.createElement("div");
        div2.className = "error";

        // запускаем функции проверки инпутов и совпадения паролей на изменения в полях ввода
        input.addEventListener("change", () => {
            if (!validateInput(input.value, pattern)) {
                div2.innerText = "Поле заполнено неверно!";
                validInput = false;
            } else {
                div2.innerText = "";
                validInput = true;
            };
        })

        // создали атрибуты для инпутов
        input.setAttribute("type", type);
        input.setAttribute("placeholder", placeholder);
        input.className = className;
        label.innerText = placeholder;
        label.append(input);
        div.append(label);
        div.append(div2);
        return div;
    }

    // создали поля для ввода и кнопку для отправки данных
    const inputs = [
        elements("text", "Введите Ваше имя   ", "", /^[А-яіїґє-]+$/),
        elements("tel", "Введите номер телефона   ", "", /^\+38\d{3}-\d{3}-\d{2}-\d{2}$/),
        elements("email", "Введите Ваш емейл   ", "", /[ˆa-z0-9._]+@[a-z0-9.-]+\.[a-z]+$/),
        elements("password", "Введите Ваш пароль   ", "pass1", /[ˆ0-9a-zA-Z!@#$%^&*]{6,}$/),
        elements("password", "Подтвердите Ваш пароль   ", "pass2", /[ˆ0-9a-zA-Z!@#$%^&*]{6,}$/),
        elements("submit")
    ]
    root.append(...inputs);

    // функция проверки полей ввода
    function validateInput(value, pattern) {
        return pattern.test(value);
    }

    // функция проверки совпадения паролей
    let pass1 = document.getElementsByClassName("pass1")[0],
        pass2 = document.getElementsByClassName("pass2")[0];

    function validatePass(value) {
        if (pass1.value === pass2.value) {
            validPass = true;
        }
        return validPass;
    }

    // создание события на проверку паролей на совпадения
    pass2.addEventListener("change", () => {
        if (!validatePass(pass2.value)) {
            alert("Пароли не совпадают!");
        }
    })

    // добавляем событие на проверку валидности формы
    let submit = document.querySelector("[type = submit")
    submit.addEventListener("click", (e) => {
        for (let i = 0; i < inputs.length; i++) {
            if (!validInput) {
                alert("Форма заполнена неверно!");
                break;
            }
        }
        if (pass1.value !== pass2.value) {
            alert("Форма заполнена неверно!");
        } else {
            localStorage();
        }
    })

    // отправляем данные в локальное хранилище
    function localStorage() {
        const name = root.querySelector("[type = text ").value;
        const tel = root.querySelector("[type = tel]").value;
        const email = root.querySelector("[type = email]").value;
        const pass = root.querySelector("[class = pass1]").value;

        localStorage.name1 = name;
        localStorage.tel = tel;
        localStorage.email = email;
        localStorage.password = pass;
        localStorage.time = new Date();

        // выводим на странице данные из локального хранилища
        output.innerHTML = localStorage.name1;
        output.innerHTML += "<br>";
        output.innerHTML += localStorage.tel;
        output.innerHTML += "<br>";
        output.innerHTML += localStorage.email;
        output.innerHTML += "<br>";
        output.innerHTML += localStorage.password;
        output.innerHTML += "<br>";
        output.innerHTML += localStorage.time;
    }
})
